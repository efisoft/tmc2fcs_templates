{{~

############################################################
# Some parameter have both int and str Hungarian annotation 
# and can result to conflict when transform to python names 
if !special_parameter_cases 
special_parameter_cases = {}
end
special_parameter_cases['sSubstate'] =  'substate_text' 
special_parameter_cases['sState'] =  'state_text' 
special_parameter_cases['sLastCommand'] = 'last_command_text' 
special_parameter_cases['sHwStatus'] = 'hw_status_text' 
special_parameter_cases['sStatus'] =  'status_text' 
special_parameter_cases['sRPCErrorText'] =  'rpc_error_text' # on shutter 

func hungarian_to_python(Name) 
# convert PLC Hungarian naming convention to python naming (e.g. lrPosActual -> pos_actual)
    if special_parameter_cases[Name]
      ret special_parameter_cases[Name]
    end
    ret Name | remove_hungarian_prefix | camel_to_snake
    # To be remove ->
    #ret Name | regex.replace  "^([a-z_]+)Arr([A-Z]+)([a-z0-9]*)$" "$2$3"  | regex.replace  "^([a-z_]+)([A-Z_]+)([a-z0-9_]*)$" "$2$3"  |  regex.replace  "^([a-z]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)$" "$2$3_$4$5" |  regex.replace  "^([a-z]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)$" "$2$3_$4$5_$6$7" |  regex.replace  "^([a-z]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)$" "$2$3_$4$5_$6$7_$8$9"  | regex.replace  "^([a-z]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9_]+)([A-Z_]+)([a-z0-9]+)([A-Z_]+)([a-z0-9_]+)$" "$2$3_$4$5_$6$7_$8$9_$10$11" | lower | regex.replace "__" "_" 
end

func camel_to_snake(Name)
# Convert CamelCase to snake_case  
   m = regex.matches Name "([A-Z][A-Z0-9_]+|[A-Z][a-z0-9_]*|^[a-z0-9_]+)"
   if array.size(m)<1
      ret Name 
   end
   m2 = [] 
   for i in m 
      m2 = m2 | array.add i[0]
   end
   ret m2 | array.join "_" | lower | regex.replace "__" "_"
end

func remove_hungarian_prefix(Name)
   pref = ["e","a", "arr", "st","ref","fb","prg","b","x","n","si","i",
          "di","li","usi","ui", 
           "udi", "f", "t", "d","td", "dt","s","ws", "c","wc","w","r", "lr",'tim'];
   for p in pref
   if array.size(regex.match Name "^"+p+"([A-Z]+.*)")>0
        ret Name | regex.replace "^"+p+"([A-Z]+.*)" "$1";
	end
   end
   ret Name  
end


func py_property_name(Property)
# return a python parameter name from parameter 
ret hungarian_to_python Property.Name
end


func py_rpc_parameter_name(Parameter)
# return a python name for a Parameter 
    ret Parameter.Name | regex.replace "^in_" "" | hungarian_to_python
end 

func py_rpc_method_name(Method) 
# return a python name for a given Method
  ret Method.Name | regex.replace "^RPC_" "" | regex.replace "^([A-Z].)([a-z]+)([A-Z].)([a-z]+)$" "$1$2_$3$4" |  regex.replace  "^([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)$" "$1$2_$3$4_$5$6" |  regex.replace  "^([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)$" "$1$2_$3$4_$5$6_$7$8"  | regex.replace  "^([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)([A-Z]+)([a-z]+)$" "$1$2_$3$4_$5$6_$7$8_$9$10"  | lower 
end 

func py_class_name(Class) 
for part in (regex.split (Class.Name | regex.replace '^(T_|FB_)' '') '_')
first_upper part
end
end


############################################################



############################################################
# Convert a Property.OpcUaType to a python type 
func _ua_type_to_py_type(OpcUaType)
	if array.size(regex.match  OpcUaType   "[U]?Int[0-9]?")>0
		ret "int"
	else if array.size(regex.match  OpcUaType   "Boolean")>0
		ret "bool" 
	else if array.size(regex.match  OpcUaType   "(Float|Double)")>0
		ret "float" 
	else if array.size(regex.match  OpcUaType   "String")>0
		ret "str" 
	else if array.size(regex.match  OpcUaType   "Byte")>0
		ret "bytes" 
	else if array.size(regex.match  OpcUaType   "DateTime")>0
		ret "datetime.datetime" 
	else 
		ret "typing.Any"
	end
end

variant_loockup = {
'1':'bool', '2':'bytes', '3':'bytes',  '4':'int', '5':'int', '6':'int', '7':'int', 
'8':'int', '9':'int', '10':'float', '11':'float', '12':'str', 
'13':'datetime.datetime', '15':'bytes'
}

func _ua_variant_to_py_type
  if variant_loockup[$0] 
    ret variant_loockup[$0]
  else
    ret 'typing.Any'
  end
end 

func py_type(Parameter)
 if Parameter.PyType 
  ret Parameter.PyType
 end 
 if Parameter.OpcUaDeclaration # This is an Rpc Method Parameter 
  ret _ua_variant_to_py_type Parameter.OpcUaTypeId 
 else 
  ret _ua_type_to_py_type Parameter.OpcUaType 
 end
end 





############################################################



############################################################
py_default_loockups = {
  float : '0.0',
  int : '0', 
  str : '""', 
  bool: 'False',
  datetime : 'datetime(1977,1,1)',
  bytes : 'b""',
}
func py_type_to_py_default(py_type) 
   if py_default_loockups[py_type] 
     ret py_default_loockups[py_type]
   else
     ret 'None'
   end
   end


func py_default(Property)
 if !Property.Default 
  ret Property | py_type | py_type_to_py_default
 end
 $pytype = Property | py_type 
 if $pytype == 'bool'
      if Property.Default == '1' 
   	  ret 'True' 
      else 
      	  ret 'False' 
      end
 end 
 if $pytype == 'str'
   ret '"'+Property.Default+'"' 	
 end
 if (regex.match Property.Default `^([+-]?\s*([0-9]+)|([+-]?\s*([0-9]+)[.]([0-9]*)))$`).size > 0
   ret  Property.Default 
 else 
   ret "'"+Property.Default+"'"
 end

end 

############################################################



############################################################
# Convert a Property to a py type annotation 
func property_to_py_type(Property)
   if Property.IsArray 
     ret property_to_py_type_array Property
   else 
     ret property_to_py_type_scalar Property
   end
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_type_array(Property) 
   if Property.OpcUaType == "ExtensionObject"
        class = ExposedClasses[Property.Type] 
	if class.ExposedProperties.size == 1 # assuming this is a lrValue, bValue etc ... 
	   ret property_to_py_type_array class.ExposedProperties[0] 
	else 
           ret property_to_py_type_custom_array Property 
	end
   else 
        ret property_to_py_type_base_array Property 
   end 	
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_type_scalar(Property) 
  if Property.OpcUaType == "ExtensionObject"
   	class = ExposedClasses[Property.Type] 
	if class.ExposedProperties.size == 1 # assuming this is a lrValue, bValue etc ... 
	   ret property_to_py_type_scalar class.ExposedProperties[0] 
	else 
           ret property_to_py_type_custom_scalar Property
	end
  else 
        ret property_to_py_type_base_scalar Property
  end 
 
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_type_base_scalar(Property)
    ret Property | py_type
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_type_base_array(Property)
    ret 'Array[' +( Property | py_type )+ ']' 
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_type_custom_scalar(Property)
    ret Property.Type
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_type_custom_array(Property)
    ret 'Array[' + Property.Type+ ']' 
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




############################################################
func property_to_py_default(Property)
   if Property.IsArray 
      property_to_py_default_array Property Property.ArraySize 	
   else 
      property_to_py_default_scalar Property
   end
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_default_array(Property, size)
    if Property.OpcUaType == "ExtensionObject"
        class = ExposedClasses[Property.Type] # assuming this is a lrValue, bValue etc ... 
	if class.ExposedProperties.size == 1
	   ret property_to_py_default_array  class.ExposedProperties[0] size 
	else 
	   ret property_to_py_default_custom_array Property size
	end 
      else 
        ret property_to_py_default_base_array Property size
      end 
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_default_scalar(Property)
    if Property.OpcUaType == "ExtensionObject"
        class = ExposedClasses[Property.Type] # assuming this is a lrValue, bValue etc ... 
	if class.ExposedProperties.size == 1
	    ret property_to_py_default_scalar class.ExposedProperties[0]	
	else 
            ret property_to_py_default_custom_scalar Property
	end 
    else 
        ret property_to_py_default_base_scalar Property
    end 
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_default_custom_array(Property, size)
     ret 'field(default_factory= lambda : Array('+ size + ','+Property.Type+'))'
end
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_default_custom_scalar(Property)
    ret 'field(default_factory= '+Property.Type+' )'
end 
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_default_base_array(Property, size)
   if !Property.Default 
     ret 'field(default_factory= lambda: Array(' + size + ','+(Property | py_type)+ '))'
   else 
     ret 'field(default_factory= lambda : Array(' + size + ', lambda :'+(Property | property_to_py_default_base_scalar)+'))'
   end 
end
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func property_to_py_default_base_scalar(Property)
   ret py_default Property
end

# ##################################################################
func py_method_input_parameter (method)  
# should receive a Method definition 
params = ''
for Param in method.In.Parameters
   params = params+ ', '+(Param | py_rpc_parameter_name)+':'+(Param | py_type)
end
ret params
end 

func py_method_output_parameter (method)
   out_params = ''
   if method.HasOut
   	if method.Out.Parameters.size>1
          out_params = 'tuple['
	  for Param in method.Out.Parameters
		out_params = out_params + (Param | py_type) + ',' 
	  end 
	else
	  out_params = method.Out.Parameters[0] | py_type 
	end
   else 
     out_params = 'None'
   end 
   ret out_params
end 

func fix_enum_name
   ret $0 | regex.replace "INITIALIZING" "INITIALISING"
end

func to_error_text  
if error_texts 
  if error_texts[$0]
  	ret error_texts[$0]
  end 
end 
ret $0
end

func to_rpc_error_text  
if rpc_error_texts 
  if rpc_error_texts[$0]
  	ret rpc_error_texts[$0]
  end 
end 
ret $0
end

~}}
{{-
#############################################################
# Build an Enum Class 
func py_enum_class (enum) 
 func fix_enum_name
   ret $0 | regex.replace "INITIALIZING" "INITIALISING"
 end
~}}
class {{enum.Name}}(enum.IntEnum):
    """
    Enumeration for {{enum.Name}} generated automaticcaly from PLC lib tmc file
    """
{{~ 
  for entry in enum.Entries
    for comment in entry.Comment 
~}}
    # {{comment}}
{{~ 
    end ~}}        
    {{upper entry.Label | fix_enum_name}} = {{entry.Value}}
{{~ 
  end ~}}
  {{~ end ~}}

