{{~##
|  Export the Application Interface for the device simulator 
|  some imports are needed and not included here :   dvs, dfn, queue, <Device>Engine, typing, ..
##~}}

@dataclass
class {{DeviceName | first_upper}}SimInterface(dvs.BaseSimInterface):
    engine: {{DeviceName | first_upper}}SimEngine = field(default_factory={{DeviceName | first_upper}}SimEngine)
    
    {{~if Compose~}}
    {{~ for Obj in (ExposedObjects | root_composition_properties) ~}}
    
    @functools.cached_property
    def  {{Obj.PyName}}_sim(self) -> {{Obj.Composition.PySimClass}}:
        return {{Obj.Composition.PySimClass}}(self.engine.{{Obj.PyName}})
    {{~end~}}
    {{~end~}}

    @classmethod
    def get_config_factory(cls) -> dvs.core.IConfigFactory[{{DeviceName | first_upper}}SimConfig]:
        """Return a config factory for {{DeviceName | first_upper}} device"""
        return {{DeviceName | first_upper}}SimConfigLoader().new_factory({{DeviceName | first_upper}}SimConfig)

    @classmethod
    def from_config(cls, config: {{DeviceName | first_upper}}SimConfig) -> {{DeviceName | first_upper}}SimInterface:
        return cls({{DeviceName | first_upper}}SimEngine(config, log=dvs.core.get_logger(config.log)))
    
    ## ############################ 
    # methods in the ISimInterface 
    #
    @classmethod 
    def get_factory(cls) -> dvs.core.IFactory[{{DeviceName | first_upper}}SimInterface]:
        return dvs.SimInterfaceFactory(cls.from_config, cls.get_config_factory())

    def get_engine(self) -> {{DeviceName | first_upper}}SimEngine:
        return self.engine 
    
    def add_to_server(self, server_manager: dvs.opcua.ServerManager) -> None:
        dvs.utils.add_to_server( 
            server_manager, self.engine.get_config(), self.engine)
        {{~if Compose~}} 
        # merge child member profile which are not exposed in the namespace
        {{~ for Obj in (ExposedObjects | root_composition_properties) ~}}
        {{~if Obj.Composition.SkipNamespace~}}
        self.{{Obj.PyName}}_sim.add_to_server(server_manager)
        {{~end~}}
        {{~end~}}
        {{~end~}} 
    
    def iter_runners(self, excludes: set[str] = set()) -> typing.Iterator[dvs.core.IRunner]:
        yield from dvs.utils.iter_runners(self, excludes)
        {{~if Compose~}} 
        # and exclude opcua an engine runner, because handled in root engine
        {{~ for Obj in (ExposedObjects | root_composition_properties) ~}}
        yield from self.{{Obj.PyName}}_sim.iter_runners(excludes | {'engine','opcua'})
        {{~end~}}
        {{~end~}}


